import 'package:flutter/material.dart';

abstract class AppColors {
  static const yellow = Color(0xFFF8D548);

  static const black = Color(0xFF2B2A28);
  static const gray = Color(0xFF666666);
}
