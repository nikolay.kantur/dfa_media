import 'package:dfa_media/styles/app_colors.dart';
import 'package:flutter/material.dart';

abstract class AppStyles {
  static TextStyle sfUiDisplay({
    required double fontSize,
    Color? color = AppColors.black,
    FontWeight? fontWeight = FontWeight.normal,
    FontStyle? fontStyle,
    double? height,
    double? letterSpacing,
  }) {
    return TextStyle(
      fontSize: fontSize,
      fontFamily: 'SF UI Display',
      color: color,
      fontWeight: fontWeight,
      fontStyle: fontStyle,
      height: height,
      letterSpacing: letterSpacing,
    );
  }

  static final appBarTitleActive = sfUiDisplay(fontSize: 10);
  static final appBarTitle = sfUiDisplay(fontSize: 8, color: AppColors.gray);
}
