import 'dart:math';

import 'package:dfa_media/styles/app_colors.dart';
import 'package:dfa_media/styles/app_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var overlayTextStyle = AppStyles.sfUiDisplay(
      fontSize: 14,
      fontWeight: FontWeight.w600,
      height: 16.71 / 14,
      color: Colors.white,
    );
    final maxTopPadding =
        max(MediaQuery.of(context).padding.top, 20).toDouble();
    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      children: [
        SizedBox(height: maxTopPadding),
        Container(
          width: 152,
          height: 152,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(152),
          ),
          child: Image.asset('assets/network/buh.png'),
        ),
        const SizedBox(height: 10),
        Text(
          'Ваш бухгалтер',
          textAlign: TextAlign.center,
          style: AppStyles.sfUiDisplay(
            fontSize: 18,
            height: 26.69 / 18,
          ),
        ),
        const SizedBox(height: 11),
        Text(
          'Наталья Анашкина',
          textAlign: TextAlign.center,
          style: AppStyles.sfUiDisplay(
            fontSize: 20,
            height: 29.66 / 20,
          ),
        ),
        const SizedBox(height: 20),
        const _Title('Уведомления'),
        const SizedBox(height: 15),
        const _CellYellow(),
        const SizedBox(height: 20),
        const _Title('Новости'),
        const SizedBox(height: 11),
        Container(
          height: 166,
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            image: const DecorationImage(
              image: AssetImage("assets/network/typing.jpg"),
              fit: BoxFit.cover,
            ),
            boxShadow: [
              BoxShadow(
                color: AppColors.black.withOpacity(0.22),
                offset: Offset.zero,
                blurRadius: 13,
              ),
            ],
          ),
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 11, horizontal: 18),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.transparent,
                  Colors.black.withOpacity(0.57),
                  Colors.black.withOpacity(0.57),
                ],
                stops: const [0, 53.65, 100],
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Spacer(),
                Text(
                  '20.03.2023 - Вебинар',
                  style: overlayTextStyle,
                ),
                // Text(
                //   'В Госдуму внесен законопроект, направленный на реформирование фывафывао флывоа рфлыва ылва лфрыва офры в ...ещё',
                //   maxLines: 2,
                //   style: overlayTextStyle,
                // ),
                RichText(
                  maxLines: 2,
                  overflow: TextOverflow.clip,
                  text: TextSpan(
                    text:
                        'В Госдуму внесен законопроект, направленный на реформирование фывафывао флывоа рфлыва ылва лфрыва офры в',
                    children: [
                      TextSpan(
                        text: '...ещё', // TODO doest cut well
                        style: overlayTextStyle.copyWith(
                          color: Colors.white.withOpacity(.58),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        const SizedBox(height: 200),
      ],
    );
  }
}

class _Title extends StatelessWidget {
  const _Title(
    this.title, {
    Key? key,
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: AppStyles.sfUiDisplay(
        fontSize: 18,
        height: 26.69 / 18,
      ),
    );
  }
}

class _CellYellow extends StatelessWidget {
  const _CellYellow({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 86,
      decoration: BoxDecoration(
        color: const Color(0xFFFDFDFD),
        borderRadius: BorderRadius.circular(20),
        boxShadow: const [
          BoxShadow(
            color: Color(0xFFFFD83C),
            offset: Offset.zero,
            blurRadius: 7,
          ),
        ],
      ),
      child: Row(
        children: [
          SvgPicture.asset('assets/network/people.svg'),
          Expanded(
            child: Text(
              'Добавьте вашу первую\n компанию',
              textAlign: TextAlign.center,
              style: AppStyles.sfUiDisplay(
                fontSize: 14.5,
                height: 17.3 / 14.5,
              ),
            ),
          ),
          const SizedBox(width: 18),
        ],
      ),
    );
  }
}
