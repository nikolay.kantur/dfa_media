import 'package:dfa_media/pages/calendar/calendar_page.dart';
import 'package:dfa_media/pages/main/main_page.dart';
import 'package:dfa_media/pages/my_companies/my_companies_page.dart';
import 'package:dfa_media/pages/profile/profile_page.dart';
import 'package:dfa_media/pages/yellow/yellow_page.dart';
import 'package:dfa_media/styles/app_colors.dart';
import 'package:dfa_media/styles/app_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

class GlobalPage extends StatefulWidget {
  const GlobalPage({
    Key? key,
  }) : super(key: key);

  @override
  State<GlobalPage> createState() => _GlobalPageState();
}

class _GlobalPageState extends State<GlobalPage> {
  int _currentIndex = 0;
  indexSelected(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.dark,
        // statusBarBrightness: Brightness.dark,
        // systemStatusBarContrastEnforced: true,
        // systemNavigationBarContrastEnforced: true,
      ),
      child: Scaffold(
        body: Container(

          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                  'assets/images/background.png'), // TODO move to strings
              fit: BoxFit.cover,
            ),
          ),
          child: IndexedStack(
            index: _currentIndex,
            children: const [
              MainPage(),
              MyCompaniesPage(),
              YellowPage(),
              CalendarPage(),
              ProfilePage(),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          height: 55 + MediaQuery.of(context).padding.bottom,
          color: Colors.white,
          padding: const EdgeInsets.only(top: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _MenuItem(
                title: 'Главная',
                icon: 'assets/icons/home.svg',
                isActive: _currentIndex == 0,
                onTap: () => indexSelected(0),
              ),
              _MenuItem(
                title: 'Мои компании',
                icon: 'assets/icons/case.svg',
                isActive: _currentIndex == 1,
                onTap: () => indexSelected(1),
              ),
              Expanded(
                child: GestureDetector(
                  onTap: () => indexSelected(2),
                  child: Stack(
                    alignment: Alignment.topCenter,
                    clipBehavior: Clip.none,
                    children: [
                      Positioned(
                        top: -12,
                        child: Container(
                          height: 51,
                          width: 51,
                          decoration: BoxDecoration(
                            color: AppColors.yellow,
                            borderRadius: BorderRadius.circular(100),
                            boxShadow: const [
                              BoxShadow(
                                color: Color.fromRGBO(113, 110, 93, 0.38),
                                blurRadius: 10,
                                offset: Offset(0, 8),
                              ),
                            ],
                          ),
                          child: Container(
                            height: 37,
                            width: 31,
                            padding: const EdgeInsets.fromLTRB(10, 5, 10, 9),
                            child: SvgPicture.asset(
                              'assets/icons/bottom_app_bar_icon.svg',
                              // color: AppColors.black,
                              height: 20,
                              width: 20,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              _MenuItem(
                title: 'Календарь',
                icon: 'assets/icons/calendar.svg',
                isActive: _currentIndex == 3,
                onTap: () => indexSelected(3),
              ),
              _MenuItem(
                title: 'Профиль',
                icon: 'assets/icons/user.svg',
                isActive: _currentIndex == 4,
                onTap: () => indexSelected(4),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _MenuItem extends StatelessWidget {
  const _MenuItem({
    Key? key,
    required this.title,
    required this.icon,
    required this.isActive,
    required this.onTap,
  }) : super(key: key);

  final String title;
  final String icon;

  final bool isActive;

  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: onTap,
        child: Column(
          children: [
            Container(
              height: 33,
              width: 33,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(50),
                boxShadow: isActive
                    ? const [
                        BoxShadow(
                          color: AppColors.yellow,
                          offset: Offset.zero,
                          blurRadius: 7,
                        ),
                      ]
                    : [],
              ),
              child: Center(
                child: SvgPicture.asset(
                  icon,
                  height: 20,
                  width: 20,
                  color: isActive ? AppColors.black : AppColors.gray,
                ),
              ),
            ),
            const SizedBox(height: 3),
            // Text
            // SF UI Display
            Text(
              title,
              style: isActive //
                  ? AppStyles.appBarTitleActive
                  : AppStyles.appBarTitle,
            ),
          ],
        ),
      ),
    );
  }
}
